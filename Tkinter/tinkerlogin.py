import Tkinter
import tkMessageBox
import readsql

class login(Tkinter.Tk):
    def __init__(self, parent):
        Tkinter.Tk.__init__(self, parent)
        self.parent = parent
        self.initalize()
        self.id = ''
        self.username = ''

    def initalize(self):
        self.grid

        self.txtuname = Tkinter.StringVar()
        lbluname = Tkinter.Label(self, text="Username :")
        lbluname.grid(column=0, row=1)
        txtuname = Tkinter.Entry(textvariable=self.txtuname)
        txtuname.grid(column=1, row=1)

        self.pword = Tkinter.StringVar()
        lblpass = Tkinter.Label(self, text="Password :")
        lblpass.grid(column=0, row=2)
        txtpass = Tkinter.Entry(textvariable=self.pword)
        txtpass.config(show="*")
        txtpass.grid(column=1, row=2)

        cmdlog = Tkinter.Button(self, text="Login",  command=self.OnCmdlogClick)
        cmdlog.grid(column=0, row=3)

        cmdreg = Tkinter.Button(self, text="Register",  command=self.OnCmdregClick)
        cmdreg.grid(column=1, row=3)

    def chooseproject(self):
        print "heer"

    def menuform(self, userid, usern, lastn, firstn):
        self.withdraw()
        self = Tkinter.Toplevel(self)
        self.title('Active Account : ' + str(lastn).upper() + " " + str(firstn).upper())
        self.geometry("700x600")

        def OnCmdprofileClick():
            print

        def OnCmdprojectsClick():
            return None

        cmdprofile = Tkinter.Button(self, text="Profile",  command=OnCmdprofileClick)
        cmdprofile.grid(column=0, row=1)

        cmdprojects = Tkinter.Button(self, text="Projects",  command=OnCmdprojectsClick)
        cmdprojects.grid(column=1, row=1)

        profiledetails = '\n\n\nID : ' + str(userid) + '\nUsername : ' + str(usern) + '\nFirstname = ' + str(firstn) + '\nLastname : ' + str(lastn)
        lblusername = Tkinter.Label(self, text=profiledetails)
        lblusername.grid(columnspan=3, row=3)

    def OnCmdregClick(self):
        self = Tkinter.Toplevel(self)
        self.title('Registration Form')

        lblu = Tkinter.Label(self, text="Username :")
        lblu.grid(column=0, row=1)

        self.txtu = Tkinter.StringVar()
        txtu = Tkinter.Entry(self, textvariable=self.txtu)
        txtu.grid(column=1, row=1)

        lblp = Tkinter.Label(self, text="Password :")
        lblp.grid(column=0, row=2)

        self.txtp = Tkinter.StringVar()
        txtp = Tkinter.Entry(self, textvariable=self.txtp)
        txtp.grid(column=1, row=2)
        txtp.config(show="*")

        lblret = Tkinter.Label(self, text="Re Type Password :")
        lblret.grid(column=0, row=3)

        self.txtret = Tkinter.StringVar()
        txtret = Tkinter.Entry(self, textvariable=self.txtret)
        txtret.grid(column=1, row=3)
        txtret.config(show="*")

        lbllname = Tkinter.Label(self, text="Lastname :")
        lbllname.grid(column=0, row=4)

        self.txtlname = Tkinter.StringVar()
        txtlname = Tkinter.Entry(self, textvariable=self.txtlname)
        txtlname.grid(column=1, row=4)

        lblfname = Tkinter.Label(self, text="Firstname :")
        lblfname.grid(column=0, row=5)

        self.txtfname = Tkinter.StringVar()
        txtfname = Tkinter.Entry(self, textvariable=self.txtfname)
        txtfname.grid(column=1, row=5)

        def OnCmdsubmit():
            u = txtu.get()
            p = txtp.get()
            ret = txtret.get()
            fname = txtfname.get()
            lname = txtlname.get()
            if u == '' or p == '':
                tkMessageBox.showinfo("Error!", "User and Password must not be blank.")
            else:
                if p == ret:
                    encryptpass = readsql.hash_password(p)
                    insertuser = readsql.executesql("insert into users(USERNAME, PASSWORD) values ('" + u + "','" + str(encryptpass) + "')")
                    readsql.executesql("insert into profiles(user_id, lastname, firstname) values ('" + str(insertuser.lastrowid) + "','" + str(lname) + "','" + str(fname) + "')")
                    tkMessageBox.showinfo("Success!", "User Added.")
                    self.withdraw()
                else:
                    tkMessageBox.showinfo("Error!", "Password and Retype Password not match.")

        cmdsubmit = Tkinter.Button(self, text="Submit",  command=OnCmdsubmit)
        cmdsubmit.grid(column=1, row=6)

    def OnCmdlogClick(self):
        user = self.txtuname.get()
        pword = self.pword.get()
        try:
            if user == '' or pword == '':
                tkMessageBox.showinfo("Error!", "Username and Password must not be blank.")
            else:
                hashpass = readsql.executesql("select * from users where username = '" + user + "'")
                hashp = hashpass.fetchone()
                if readsql.check_password(hashp[2], pword):
                    userprofile = readsql.executesql("select lastname, firstname from profiles where user_id = '" + str(hashp[0]) + "'")
                    userprof = userprofile.fetchone()
                    self.menuform(hashp[0], hashp[1], userprof[0], userprof[1])
                else:
                    tkMessageBox.showinfo("Error!", "Access Denied! wrong Password")
        except TypeError:
            tkMessageBox.showinfo("Error!", "User does not exist!")

if __name__ == "__main__":
    logform = login(None)
    logform.title("Login")
    logform.mainloop()
