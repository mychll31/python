from __future__ import division
# -*- coding: utf-8 -*-
import Tkinter
import tkMessageBox

class conversion(Tkinter.Tk):
    def __init__(self, parent):
        Tkinter.Tk.__init__(self, parent)
        self.parent = parent
        self.initalize()

    def initalize(self):
        self.grid

        self.cel = Tkinter.StringVar()
        self.fah = Tkinter.StringVar()
        self.kel = Tkinter.StringVar()

        Tkinter.Label(self, text="TEMPERATURE").grid(column=0, row=1)
        Tkinter.Label(self, text="Celcius       "+u"\u00b0"+"c : ").grid(column=0, row=2)
        Tkinter.Label(self, text="Fahrenheit "+u"\u00b0"+"f : ").grid(column=0, row=3)
        Tkinter.Label(self, text="Kelvin        "+u"\u00b0"+"k : ").grid(column=0, row=4)

        Tkinter.Entry(textvariable=self.cel).grid(column=1, row=2)
        Tkinter.Entry(textvariable=self.fah).grid(column=1, row=3)
        Tkinter.Entry(textvariable=self.kel).grid(column=1, row=4)

        Tkinter.Button(self, text="Convert",  command=self.OnCmdConvert).grid(column=0, row=5)
        Tkinter.Button(self, text="Clear",  command=self.OnCmdClear).grid(column=1, row=5)

    def OnCmdClear(self):
        self.cel.set("")
        self.fah.set("")
        self.kel.set("")

    def OnCmdConvert(self):
        cel = self.cel.get()
        fah = self.fah.get()
        kel = self.kel.get()
        empty = 0
        notempty = ''

        if cel != '':
            empty += 1
            notempty = 'cel'
        if fah != '':
            empty += 1
            notempty = 'fah'
        if kel != '':
            empty += 1
            notempty = 'kel'

        if empty > 1 or empty == 0:
            tkMessageBox.showinfo("Error!", "Please fill one textbox only. Thank you")
        else:
            if notempty == 'cel':
                try:
                    cel = float(cel)
                    resfah = cel * (9/5) + 32
                    reskel = cel + 273.15
                    self.fah.set('%.4f' % resfah)
                    self.kel.set('%.4f' % reskel)
                except ValueError:
                    tkMessageBox.showerror("Error", "Could not convert "+str(cel))
            elif notempty == 'fah':
                try:
                    fah = float(fah)
                    rescel = (fah-32) * (5/9)
                    reskel = (fah + 459.67) * (5/9)
                    self.cel.set('%.4f' % rescel)
                    self.kel.set('%.4f' % reskel)
                except ValueError:
                    tkMessageBox.showerror("Error", "Could not convert "+str(fah))
            elif notempty == 'kel':
                try:
                    kel = float(kel)
                    rescel = kel - 273.15
                    resfah = (kel * (9/5)) - 459.67
                    self.cel.set('%.4f' % rescel)
                    self.fah.set('%.4f' % resfah)
                except ValueError:
                    tkMessageBox.showerror("Error", "Could not convert "+str(kel))


if __name__ == "__main__":
    convform = conversion(None)
    convform.title("Metric Conversion")
    convform.mainloop()