load data
infile 'C:\Users\malcorin\Documents\PY\PDFminer\details.txt'
badfile 'C:\Users\malcorin\Documents\PY\PDFminer\bad.bad'
into table PDF_HEADER
APPEND
fields terminated by "," optionally enclosed by '"'
(NAME,STMTDATE DATE "YYYY-MM-DD")
into table PDF_DETAILS
APPEND
fields terminated by "," optionally enclosed by '"'
(HEADER_ID, POLICY_NO, INSURED_NAME, COMM_AMOUNT)