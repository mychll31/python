import re
import dbconnect
import timestring

def createctlfile():
    ctlfile = open("details.ctl", "w")
    ctlfile.write("load data\n")
    ctlfile.write("infile 'C:\Users\malcorin\Documents\PY\PDFminer\details.txt'\n")
    ctlfile.write("badfile 'C:\Users\malcorin\Documents\PY\PDFminer\\bad.bad'\n")
    ctlfile.write("into table PDF_HEADER\n")
    ctlfile.write("APPEND\n")
    ctlfile.write("fields terminated by \",\" optionally enclosed by '\"'\n")
    ctlfile.write("(NAME,STMTDATE DATE \"YYYY-MM-DD\")\n")
    ctlfile.write("into table PDF_DETAILS\n")
    ctlfile.write("APPEND\n")
    ctlfile.write("fields terminated by \",\" optionally enclosed by '\"'\n")
    ctlfile.write("(HEADER_ID, POLICY_NO, INSURED_NAME, COMM_AMOUNT)")
    ctlfile.close()

def loadsqlloader():
    import subprocess
    subprocess.call(['sqlldr','dbtest/dbtest@pidev control=\'C:\Users\malcorin\Documents\PY\PDFminer\details.ctl\' log=\'C:\Users\malcorin\Documents\PY\PDFminer\log.log\''])

p = re.compile("^\d+(\.\d{2})?$")
str_stmtdate = 'Statement'
str_carrname = 'Carrier'
matchfound = False
str_policy = 'Policy Number'
divisor = 3

arr_details = []
incre = 1

headerincre=0


with open('output.txt') as f:
    contents = f.readlines()

for content in contents:
    if ':' in content:
        contentcol = 0
        contentcol = content.index(':')
        if headerincre == 0:
            try:
                giventime = timestring.Date(content[contentcol+2:])
                car_date = str(giventime.year) + '-' + str(giventime.month) + '-' + str(giventime.day)
            except timestring.TimestringInvalid:
                print 'Date is invalid'
        elif headerincre == 1:
            car_name = content[contentcol+2:]
            car_name = car_name.replace(" \n", "")
        headerincre += 1
    else:
        # remove white spaces
        string = content.replace(" \n", "")
        string = string.replace("\n", "")
        string = string.replace("\x0c", "")
        # regex checking
        m = p.match(string)
        if str_policy in content:
            incre = 0
        if incre == 0 and string != '':
            arr_details.extend([string])

lastid = dbconnect.getlastid()

common = len(arr_details) / divisor
head_ctr = 0
txtfile = open("details.txt", "w")
for i in range(common-1):
    if head_ctr == 0:
        headname = car_name
        headdate = car_date
    else:
        headname = ''
        headdate = ''
    head_ctr =+ 1
    txtfile.write('"'+ headname +'","' + headdate + '","' + str(lastid+1) + '","' + str(arr_details[i+1]) + '","' + str(arr_details[i+common+1]) + '","' + str(arr_details[i+common+common+1]) + '"\n')

txtfile.close()
createctlfile()
loadsqlloader()

# checking bad file
import glob
bad = False
files = glob.glob("C:\Users\malcorin\Documents\PY\PDFminer\*")
for file in files:
    if 'bad' in file:
        bad = True
if bad == True:
    print 'some Rows not loaded due to data errors. please check bad.bad file'
else:
    print 'all Rows successfully loaded.'