def jp():
    yourturn=""
    while yourturn!="exit":
        yourturn= raw_input("Select your turn: Rc Sc St Pa: ")
        compturn=rolljp()
        print 'computer : '+ compturn + ' Your Turn : ' + yourturn + ' ' + wl(yourturn,compturn)

def rolljp():
    from random import randint
    roll= randint(1,3)
    if roll==1:
        return "Rc"
    elif roll==2:
        return "Sc"
    elif roll==3:
        return "Pa"
    else:
        return "Did\'nt find any"
    
 
def wl(mine,comp):
    if mine=="Rc":
        if comp=="Rc":
            return "Draw"
        elif comp=="Pa":
            return "You lose"
        else:
            return "You win"
    elif mine=="Sc":
            if comp=="Sc":
                return "Draw"
            elif comp=="Rc":
                return "You lose"
            else:
                return "You win"
    elif mine=="Pa":
            if comp=="Pa":
                return "Draw"
            elif comp=="Sc":
                return "You lose"
            else:
                return "You win"
    else:
        return "No turn for mine"

print jp()
