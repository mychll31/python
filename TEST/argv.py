# python "C:\Users\malcorin\Documents\PHYON\te.py" 1 2 3
from sys import argv
"""
script, first, second, third = argv

print "The script is called:", script
print "Your first variable is:", first
print "Your second variable is:", second
print "Your third variable is:", third

# python "C:\Users\malcorin\Documents\PHYON\argv.py" "mai"

script, user_name = argv
prompt = '> '

print "Hi my name is %s " % (user_name)

#C:\Python27\python "C:\Users\malcorin\Documents\PHYON\argv.py" "C:\Users\malcorin\Documents\PHYON\samp.txt"
"""
script, filename = argv
text = open(filename)
print text.read()
raw_input("?")
"""
script, filename = argv

print "We're going to erase %r." % filename
print "If you don't want that, hit CTRL-C (^C)."
print "If you do want that, hit RETURN."

raw_input("?")

print "Opening the file..."
target = open(filename, 'w')

print "Truncating the file.  Goodbye!"
target.truncate()

print "Now I'm going to ask you for three lines."

line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")

print "I'm going to write these to the file."

target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

print "And finally, we close it."
"""
target.close()
