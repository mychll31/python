"""
from openpyxl import Workbook
import time

today = (time.strftime("%d%m%Y_%H%M%S"))
wb = Workbook()
ws = wb.active
dest_filename = 'EXCL_'+ today +'.xlsx'
from openpyxl.writer.dump_worksheet import WriteOnlyCell
from openpyxl.comments import Comment
from openpyxl.styles import Style, Font
cell = WriteOnlyCell('A1', value="hello world")
cell.style = Style(font=Font(name='Courrier', size=36))
cell.comment = Comment(text="A comment", author="Author's Name")
ws = wb.create_sheet()
wb.save(filename = dest_filename)
"""

from datetime import datetime
def isoparse(s):
    try:
        return datetime(int(s[0:4]),int(s[5:7]),int(s[8:10]),
                        int(s[11:13]), int(s[14:16]), int(s[17:19]))
    except:
        return None

print isoparse('march 12, 2015')