import dbcrud

def start():
    action = ""
    while action!="exit":
        action = raw_input("action type [view, insert, update, delete or exit]: ")
        if action.lower()=="view":
            viewid = raw_input("UserID : ")
            print dbcrud.viewuser(viewid)
        elif action.lower()=="insert":
            username = raw_input("Username : ")
            password = raw_input("password : ")
            lastname = raw_input("Lastname : ")
            firstname = raw_input("Firstname : ")
            dbcrud.insertuser(username,password)
            print "inserting..."
            dbcrud.insertprofiles(firstname,lastname)
            print "user has been created"
            
        elif action.lower() == "update":
            update_userid = raw_input("user id: ")
            print dbcrud.viewuser(update_userid)

            u_username = raw_input("Username : ")
            u_password = raw_input("password : ")
            u_firstname = raw_input("Firstname : ")
            u_lastname = raw_input("Lastname : ")
            print "updating..."
            dbcrud.updateuser(update_userid,u_username,u_password,u_firstname,u_lastname)
            print "user has been updated"

        elif action.lower()=="delete":
            delete_id = raw_input("user to delete : ")
            confirm = raw_input("are you sure you want to delete " + str(delete_id) + " ? Y/N : ")
            if confirm.lower() == "y":
                print "deleting..."
                dbcrud.deleteuser(delete_id)
            print "user has been deleted"
        elif action.lower() == "exit":
            print "bye..."
        else:
            print "invalid input"

start()
    









