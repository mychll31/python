import sys

def man(wrong):
    if wrong >= 1:
        print " O"
    if wrong >= 2:
        sys.stdout.write("/")
    if wrong >= 3:
        sys.stdout.write("|")
    if wrong >= 4:
        sys.stdout.write("\\")
    if wrong >= 5:
        print
        sys.stdout.write("/")
    if wrong >= 6:
        sys.stdout.write(" \\")

man(6)