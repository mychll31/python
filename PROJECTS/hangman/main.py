import sys
import random
import os

def man(wrong):
    if wrong >= 1:
        print " O"
    if wrong >= 2:
        sys.stdout.write("/")
    if wrong >= 3:
        sys.stdout.write("|")
    if wrong >= 4:
        sys.stdout.write("\\")
    if wrong >= 5:
        print
        sys.stdout.write("/")
    if wrong >= 6:
        sys.stdout.write(" \\")

def question():
    qtxt = open(str(os.path.dirname(os.path.realpath(__file__))) + '\questions.txt', 'r')
    ques = qtxt.read().split(';')
    pfile = len(ques)-2
    for i in xrange(1):
        randques = random.randint(1, int(pfile))
    for q in ques:
        quest = q.split('|')
        if str(quest[0]) == str(randques):
            print "Clue: " + str(quest[1]) + " " + ("_ " * len(quest[2]))
            return str(quest[2])

if __name__ == "__main__":
    que = question().lower()
    ans = []
    wrongans = 0
    correctans = 0
    while wrongans < 6 and correctans < len(que):
        guess = raw_input("GUESS[one char only]: ").lower()
        while len(guess) > 1 or str(guess) == '' or str(guess) == ' ':
            print "Please input one char only"
            guess = raw_input("GUESS[one char only]: ")

        if guess.lower() in que.lower():
            ans.append(guess)
            correctans += que.count(guess)
            for questi in que:
                if [s for s in ans if questi in s]:
                    sys.stdout.write(questi.upper() + ' ')
                else:
                    sys.stdout.write(' _ ')
        else:
            wrongans += 1
            man(wrongans)
        print
    if wrongans == 6:
        print "\nYOU LOSE!!"
    else:
        print "\nYOU WIN!! SCORE " + str(correctans) + " OUT OF " + str(correctans+wrongans)
        if wrongans != 0:
            print "YOU HAVE " + str(wrongans) + " MISTAKES."
        else:
            print "PERFECT SCORE"



