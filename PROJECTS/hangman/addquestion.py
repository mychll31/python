import os

def addques():
    qtxt = open(str(os.path.dirname(os.path.realpath(__file__))) + '\questions.txt', 'r')
    ques = qtxt.read().split(';')
    qtxt = open(str(os.path.dirname(os.path.realpath(__file__))) + '\questions.txt', 'a+')
    lenques = len(ques) - 2
    action = ''
    while str(action).lower() != 'exit':
        action = raw_input("[enter] to write; [exit] to quit: ")
        if str(action).lower() != 'exit':
            clue = raw_input("Clue : ")
            answer = raw_input("Answer: ")
            lenques += 1
            qtxt.write(str(lenques) + '|')
            qtxt.write(str(clue) + '|')
            qtxt.write(str(answer))
            qtxt.write(';')
            print 'Question added...'
    qtxt.close()

def repques():
    replace = False
    while replace is not True:
        try:
            qid = int(raw_input("ID :"))
            replace = True
        except ValueError:
            print "Invalid ID no"

    if replace is True:
        qtext = str(open(str(os.path.dirname(os.path.realpath(__file__))) + '\questions.txt', 'r').read())
        lines = qtext.split(';')
        print lines

if __name__ == "__main__":
    faction = raw_input("Add = [a]; Replace = [r]: ")
    if faction.lower() == 'a':
        addques()
    elif faction.lower() == 'r':
        repques()

