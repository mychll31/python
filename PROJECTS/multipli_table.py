class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'\

def main(number, y):
    res_row = ''
    res_all = []
    for x in range(y, number+y, y):
        for n in range(y, number+y, y):
            if n <= number:
                res_row = res_row + ' ' + str(n*x)
        res_all.append(res_row)
        res_row = ''
    ctr = 1
    for res in res_all:
        ctr = ctr + 1
        if ctr % 2 == 0:
            print bcolors.OKBLUE + res
        else:
            print bcolors.OKGREEN + res

if __name__ == "__main__":
    main(10, 1)