import statistics   #python -m pip install statistics (in windows)
import sys,  traceback

def genumlist():
    num = 0
    nums = []
    q = True
    ctr = 0
    while q:
        ctr =+ 1
        try:
            num = eval(raw_input("enter number [press any non numeric char to quit]: "))
            nums.append(num)
        except NameError:
            q = False
        except SyntaxError:
            q = False
        except TypeError:
            q = False
    return nums

def main():
    try:
        numlist = genumlist()
        res_mean = statistics.mean(numlist)
        res_mode = statistics.mode(numlist)
        res_median = statistics.median(numlist)
        print 'Statistics Import'
        print '------------------'
        print 'MEAN : ' + str(res_mean)
        print 'MODE : ' + str(res_mode)
        print 'MEDIAN : ' + str(res_median)
        sys.exit(0)
    except KeyboardInterrupt:
        print "Shutdown requested...exiting"
    except Exception:
        traceback.print_exc(file=sys.stdout)

if __name__ == "__main__":
    main()

"""
sum = 0
print ctr
for n in nums:
    if n is not None:
        sum = sum + n
        mean = (mean)/(ctr-1)

print mean
print sum
"""