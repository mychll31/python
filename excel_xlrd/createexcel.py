import xlsxwriter
import time
import cx_Oracle

today = (time.strftime("%d%m%Y_%H%M%S"))

dsn = cx_Oracle.makedsn(host='59.2.0.80',sid='pidev', port=1522)
conn = cx_Oracle.connect(user='dbtest', password='dbtest', dsn=dsn)
cur = conn.cursor()
cur.execute("select * from books")

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook('EXL'+today+'.xlsx')
worksheet = workbook.add_worksheet()

# Start from the first cell. Rows and columns are zero indexed.
row = 0
col = 0

for rowvalue in cur:
    worksheet.write(row, col,     rowvalue[0])
    worksheet.write(row, col + 1, rowvalue[1])
    worksheet.write(row, col + 2, rowvalue[2])
    worksheet.write(row, col + 3, rowvalue[3])
    row += 1

print "Excel Exported"
workbook.close()

