from twilio.rest import TwilioRestClient
import sqlite3
import sys

try:
    con = sqlite3.connect('contacts.db')
    cur = con.cursor()
except:
    print 'Opps! Something wrong happen!'
    sys.exit(0)

cur.execute('SELECT * FROM contacts')
data = cur.fetchall()
# Your Account Sid and Auth Token from twilio.com/user/account
account_sid = "AC535760763f2ab6df5a454460525b0f7f"
auth_token = "e4a4e65d849bc01b8271b79d28845a48"

# variables
recipient = ''


def send(recipient):
    # TWILIO ACCOUNT
    client = TwilioRestClient(account_sid, auth_token)
    txt_message = raw_input('ENTER MESSAGE: ')
    client.sms.messages.create(body=str(txt_message), to='+63' + str(recipient), from_="+16785821253")
    print 'Text Message Successfully sent!'

while not (any(recipient.upper() in sublist for sublist in data)) and (recipient.isdigit() is False or 10 != len(recipient)):
    recipient = raw_input('Enter recipient\'s 10 digit number or name: ')

if recipient.isdigit() and len(recipient) == 10:
    send(recipient)
else:
    for contact in data:
        if recipient.upper() in contact:
            send(contact[2])
