# import smtp package
import smtplib

# Connecting to an SMTP Server
# to call the methods that log you in and send emails
# If the smptlib.SMTP() call is not successful, your SMTP server might not support TLS on port 587.
# In this case, you will need to create an SMTP object using smtplib.SMTP_SSL() and port 465 instead.
smtpObj = smtplib.SMTP('smtp.gmail.com:587')
# If you are not connected to the Internet, Python will raise a socket.gaierror: [Errno 11004]
# getaddrinfo failed or similar exception.

# ehlo() method to 'say hello' to the SMTP email server.
# This greeting is the first step in SMTP and is important for establishing a connection to the server
smtpObj.ehlo()

# This required step enables encryption for your connection.
# If you are connecting to port 465 (using SSL), then encryption is already set up, and you should skip this step.
# starttls() puts your SMTP connection in TLS mode
smtpObj.starttls()

# log in with your username and email password by calling the login() method.
smtpObj.login('mychll31@gmail.com', 'asdfasdf123')

# call the sendmail() method to actually send the email
# The start of the email body string must begin with 'Subject: \n' for the subject line of the email.
# The '\n' newline character separates the subject line from the main body of the email.
smtpObj.sendmail('mychll31@gmail.com', 'asdfme123@gmail.com', 'Subject: Solong.\nDear Alice, so long and thanks for all the fish. Sincerely, asdf')

# Be sure to call the quit() method when you are done sending emails.
# This will disconnect your program from the SMTP server.
smtpObj.quit()