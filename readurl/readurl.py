import glob
import readsql
import datetime
from datetime import date, timedelta

dateyesterday = date.today() - timedelta(days=1)
datetoday = datetime.date.today().strftime("%Y-%m-%d")

# check text file today if existing
files = glob.glob("*")
txtfile = str('htmltext'+str(datetoday)+'.txt')

# remove text file yesterday
"""
import os
yestertxtfile = str('htmltext'+str(dateyesterday)+'.txt')
if yestertxtfile in files:
    os.remove(yestertxtfile)
    print str(yestertxtfile) + " has removed."
"""

# if text file today is not existing, create one
if txtfile not in files:
    import urllib2
    response = urllib2.urlopen('http://php.fxexchangerate.com/')
    html = response.read()
    txt = open(txtfile, 'w')
    txt.write(str(html))
    txt.close()
    print "Text file created."

# open text file
text = open('htmltext'+str(datetoday)+'.txt')
html = text.read()
page = str(html)
updatedate = html.find('updated at ')
print "------------------------------------"
print page[updatedate:updatedate + 32]
print "------------------------------------"

flagars = html.find('Country')
ratedetails = page[flagars:updatedate]
contents = ratedetails.split('<div class')
td1 = ''
td2 = 0
# delete the contents of table
readsql.executesql2("delete from forex")
readsql.executesql2("vacuum")

# insert data to table
for data in contents:
    if 'class="header' not in data:
        htdata = data.split('href')
        for rawdata in htdata:
            cutend = rawdata.find('">')
            tddata = rawdata[cutend+2:].split('<td>')
            ctrtd = 0
            if len(tddata) == 4:
                for td in tddata:
                    ctrtd = ctrtd + 1
                    td = td.replace('</a></td>', '')
                    td = td.replace('</td>', '')
                    td = td.replace('<a', '')
                    td = td.replace('\n', '')
                    if ctrtd == 1:
                        td1 = td
                    elif ctrtd == 2:
                        td2 = td
                readsql.executesql2("insert into forex(country, exchange ) values ('" + td1 + "', '" + td2 + "')")
                print str(td1) + " inserted."