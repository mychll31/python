import time
import cx_Oracle
from openpyxl import Workbook
from openpyxl.styles import colors

dsn = cx_Oracle.makedsn(host='59.2.0.80',sid='pidev', port=1522)
conn = cx_Oracle.connect(user='dbtest', password='dbtest', dsn=dsn)
cur = conn.cursor()

today = (time.strftime("%d%m%Y_%H%M%S"))
wb = Workbook()
dest_filename = 'XCL\EXCL_'+ today +'.xlsx'

ws = wb.active

cur.execute("select * from books")
ctrrow = 1
btemp = 0
bactual_width = 0
ctemp = 0
cactual_width = 0

for row in cur :
    a = str(row[0]) if type(row[0]) is str else int(row[0])
    ws.cell('%s%s'%('A', ctrrow)).value = a
    b = str(row[1]) if type(row[1]) is str else int(row[1])
    ws.cell('%s%s'%('B', ctrrow)).value = b
    c = str(row[2]) if type(row[2]) is str else int(row[2])
    ws.cell('%s%s'%('C', ctrrow)).value = c

    d = str(row[3]) if type(row[3]) is str else int(row[3])
    ws.cell('%s%s'%('D', ctrrow)).value = d

    """CELL WIDTH"""
    if ctemp > len(row[1]):
        bactual_width = btemp
    else:
        btemp = len(row[1])

    if ctemp > len(row[2]):
        cactual_width = ctemp
    else:
        ctemp = len(row[2])

    ctrrow = ctrrow +1
    ws.column_dimensions["B"].width = bactual_width + 2
    ws.column_dimensions["C"].width = cactual_width + 2
    """CELL WIDTH"""

ws = wb.create_sheet()
wb.save(filename = dest_filename)
