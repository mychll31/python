import openpyxl
from openpyxl import styles, cell

xfile = 'rbooks'
def writeExcel():
    workbook = openpyxl.load_workbook(filename=xfile + '.xlsx', use_iterators=True)
    worksheet = workbook.get_sheet_by_name('Sheet1')
    cols = worksheet.get_highest_column()

    for row in worksheet.iter_rows():
        for col in range(cols):
            print str(row[col].value)

if __name__ == "__main__":
    writeExcel()

"""
xfile = 'rbooks'
def writeExcel():
    try:
        file = open(xfile + '.txt', 'w')
        workbook = openpyxl.load_workbook(filename=xfile + '.xlsx', use_iterators=True)
        worksheet = workbook.get_sheet_by_name('Sheet1')
        cols = worksheet.get_highest_column()

        for row in worksheet.iter_rows():
            for col in range(cols):
                file.write('"' + str(row[col].value) + '", ')
            file.write("\n")

        file.close()

    except:
        print "can\'t create text file please check you excel file if it is really exist."

writeExcel()
"""