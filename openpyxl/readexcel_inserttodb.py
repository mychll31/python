import openpyxl
import dbcrud
from openpyxl.compat import range

choice = ""
while choice.lower()!="exit":
    choice = raw_input("type [RE] Read Excel, [IE] insert using Excel, [E] exit : ")
    try:
        if choice.lower() == 're' or choice.lower() == 'ie':
            fname = raw_input("Filename with extension : ")
            workbook = openpyxl.load_workbook(filename = fname, use_iterators = True)
            worksheet = workbook.get_sheet_by_name('Sheet1')
            totalcols = worksheet.get_highest_column()
            if choice.lower()=="re" :
                for row in worksheet.iter_rows():
                    for i in range (totalcols):
                        print row[i].value
            elif choice.lower()=="ie":
                for row in worksheet.iter_rows():
                    dbcrud.insertbooks(row[1].value,row[2].value,row[3].value)
                    print "book " + str(row[1].value)+ " by " + str(row[2].value) + " in " + str(row[3].value) + " has been added."
        else:
            print "invalid choice"
    except:
        print "file not found!"
