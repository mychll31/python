import time
import cx_Oracle

from openpyxl import Workbook

dsn = cx_Oracle.makedsn(host='59.2.0.80',sid='pidev', port=1522)
conn = cx_Oracle.connect(user='dbtest', password='dbtest', dsn=dsn)
cur = conn.cursor()

today = (time.strftime("%d%m%Y_%H%M%S"))
wb = Workbook()
dest_filename = 'EXCL_'+ today +'.xlsx'

ws = wb.active

cur.execute("select * from books")
ctrrow = 1
btemp = 0
bactual_width = 0
ctemp = 0
cactual_width = 0

for row in cur :
    ws.cell('%s%s'%('A', ctrrow)).value = int(row[0])
    ws.cell('%s%s'%('B', ctrrow)).value = str(row[1])
    ws.cell('%s%s'%('C', ctrrow)).value = str(row[2])
    ws.cell('%s%s'%('D', ctrrow)).value = int(row[3])

    """CELL WIDTH"""
    if ctemp > len(row[1]):
        bactual_width = btemp
    else:
        btemp = len(row[1])

    if ctemp > len(row[2]):
        cactual_width = ctemp
    else:
        ctemp = len(row[2])

    ctrrow = ctrrow +1
    ws.column_dimensions["B"].width = bactual_width + 2
    ws.column_dimensions["C"].width = cactual_width + 2
    """CELL WIDTH"""

ws = wb.create_sheet()
wb.save(filename = dest_filename)