Page 1
Statement Date:   March 13, 2015
Carrier Name:     John Hay Doe
Policy Number     Insured Name      Commission Amount
Included
A1001             Hellen Mckinley   23908.76
A1002             Kelly Heler       1249.13
A1003             Jessie Mcguire    2321.54
                                    ______________
                                    27479.43
Page 2
Policy Number     Insured Name      Commission Amount
Included
A1004             Jasper Jean Lo    3321.04
A1005             Lizzie Doe        44900.07
A1006             Charmagne Uy      2321.00
                                    ______________
                                    50542.11
Skip
A1007             Neil Armstrong    2200.00
A1008             Joanne Lim        1002.45
A1009             Jenner Kelly      990.99
                                    ______________
                                    4193.44
Included
A1010             Janice Alba       9034.51
A1011             Alice Aniston     13456.09
A1012             Stacy Pitt        5408.23
                                    ______________
                                    27898.83
