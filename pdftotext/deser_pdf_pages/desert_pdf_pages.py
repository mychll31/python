import subprocess
import time
import cx_Oracle

dsn = cx_Oracle.makedsn(host='59.2.0.80', sid='pidev', port=1522)
conn = cx_Oracle.connect(user='dbtest', password='dbtest', dsn=dsn)
cur = conn.cursor()

path = 'C:\Users\malcorin\Documents\PY\pdftotext'

def sliceword(content):
   if ':' in content:
       contentcol = content.index(':')
       return content[contentcol+2:]

def insertheader(carname,cardate):
    cur.execute("insert into PDF_HEADER (NAME,STMTDATE) values ('" + carname + "', TO_DATE('" + cardate + "','YYYY-MM-DD'))")
    conn.commit()
    return "Header successfully added"

def getlastid():
    cur.execute("select id from  (select * from PDF_HEADER order by id desc) where ROWNUM <= 1")
    for row in cur :
        return row[0]

def createtxt(txtname):
    with open(txtname) as f:
        contents = f.readlines()
    txtfile = open('ctl_desert_pdf_pages.txt', 'w')
    pages = 0
    for content in contents:
        if 'Name' in content and 'Flavor' in content and 'Date Manufacture' in content:
            pages = pages + 1

        content = content.replace("\n", "")
        content = content.replace("  ", "\",\"")
        content = content.replace("\"\",", "")
        content = content.replace("\",\" ", "\",\"")
        content = content.replace("  ", "")
        content = '"' + str(content) + '"'
        content = content.replace("\" ", "\"")
        txtfile.write(content + '\n')
    print 'Total Page/s : ' + str(pages)
    txtfile.close()

def createctl():
    ctlfile = open("details.ctl", "w")
    ctlfile.write("load data\n")
    ctlfile.write("infile '" + path + "\ctl_output.txt'\n")
    ctlfile.write("badfile '" + path + "\\bad.bad'\n")
    ctlfile.write("into table PDF_DETAILS\n")
    ctlfile.write("APPEND\n")
    ctlfile.write("fields terminated by \",\" optionally enclosed by '\"'\n")
    ctlfile.write("(POLICY_NO, INSURED_NAME, COMM_AMOUNT)")
    ctlfile.close()

def loadsqlloader():
    subprocess.call(['sqlldr', 'dbtest/dbtest@pidev control=\'' + path + '\details.ctl\' log=\'' + path + '\log.log\''])
    return 'details successfully added'

def recourdcount():
    cur.execute("select count(distinct id) from PDF_DETAILS")
    for row in cur:
        return row[0]

def totalamount():
    cur.execute("select sum(distinct comm_amount) from PDF_DETAILS")
    for row in cur:
        return row[0]

def updateheader(id, rc, ta):
    cur.execute("update PDF_HEADER set recordcnt = %i, totalamount = %s where id = %i" % (rc, ta, id))
    conn.commit()
    return "Header successfully updated."

def updatedetails(id):
    cur.execute("update PDF_DETAILS set header_id = %i where header_id is NULL" % id)
    conn.commit()
    return "Details successfully updated."

print 'Convert PDF to TEXT'
pdfname = 'desert_pdf_pages.pdf'
txtname = 'desert_pdf_pages.txt'
print createtxt(txtname)

#print updateheader(getlastid(), recourdcount(), totalamount())
#print updatedetails(getlastid())
