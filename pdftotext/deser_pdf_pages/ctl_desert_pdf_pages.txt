"No","Name","Flavor","Date Manufacture","Weight"
"1","Chocolate Cake","Chocolate","10-Jan-12","10.5"
"2","Coffee Jelly","Coffee","18-Oct-07","12.5"
"3","Teramisu","Chocolate","12-Oct-05","124.75"
"4","Sansrival","Nuts","3-Feb-06","45.5"
"5","Blue Berry Cheese Cake","Blue Berry and Cheese","5-May-05","50.88"
"6","Ice Cream Cake","Ice Cream","22-Mar-11","10.1"
"7","Choco Delight","Chocolate","8-May-14","12.12"
"8","Royal Ice Cream","Ice Cream","1-May-14","51.2"
"9","Ice Cream Sandwich","Ice Cream","22-Jun-15","10.07"
"10","Coffee Crumble","Coffee","6-Oct-12","10.1"
"No","Name","Flavor","Date Manufacture","Weight"
"1","Mocha","Mocha","10-Jan-12","10.5"
"2","White Chocolate","Chocolate","18-Oct-07","12.5"
"3","Black Chocolate","Chocolate","12-Oct-05","124.75"
"4","Almond","Nuts","3-Feb-06","45.5"
"5","Red Velvet","Ice Cream","5-May-05","50.88"
"6","Mango Ice Cream","Ice Cream","22-Mar-11","10.1"
"7","Ube Ice Cream","Ice Cream","8-May-14","12.12"
"8","Vanilla Cream","Ice Cream","1-May-14","51.2"
"9","Macapuno Ice Cream","Ice Cream","22-Jun-15","10.07"
"10","Black Coffee","Coffee","6-Oct-12","10.1"
""
