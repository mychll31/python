import openpyxl
import datetime

def getexcel(xfile):
    workbook = openpyxl.load_workbook(filename=xfile, use_iterators=True, data_only=True)
    worksheet = workbook.get_sheet_by_name('ACSIA INC')
    printhis = False
    rowcount = 0
    for row in worksheet.iter_rows():
        rowstring = ''
        for cell in row:
          if cell.internal_value is not None:
            c = cell.internal_value
          else:
            c = ''
          rowstring = rowstring + "\""+ str(c) + "\", "
        rowcount = rowcount + 1
        if '"", "", "", "", "", "", "", ""' not in rowstring:
          print rowstring


print getexcel('ACSIA_013016.xlsx')
