import cx_Oracle
import openpyxl
import subprocess
import time
import datetime
today = (time.strftime("%d%m%y_%H%M%S"))
path = 'C:\Users\malcorin\Documents\PY\pdftotext\\acsia\\'
global fileid
fileid = int(raw_input('FILE ID : '))

def connect():
    dsn = cx_Oracle.makedsn(host='59.2.0.80', sid='pidev', port=1522)
    conn = cx_Oracle.connect(user='dbtest', password='dbtest', dsn=dsn)
    cur = conn.cursor()
    return cur, conn
cur, conn = connect()

def getfilename(fileid):
    cur.execute("SELECT m_carrierfiles.filename || '.' || carrier_feed.format as \"fname\" FROM m_carrierfiles INNER JOIN carrier_feed ON m_carrierfiles.feedid = carrier_feed.feedid WHERE processstatus is NULL and FILEID = " + str(fileid))
    filename = cur.fetchone()
    return filename[0]

def fetchblob(fname, fid):
    cur.execute("select carfile from m_carrierfiles where fileid = " + str(fid))
    row = cur.fetchone()
    with open('out_' + fname, 'wb') as f:
        f.write(str(row[0]))

def getexcel(xfile):
    workbook = openpyxl.load_workbook(filename=xfile, use_iterators=True)
    worksheet = workbook.get_sheet_by_name('ACSIA INC')
    printhis = False
    #print worksheet['G12'].value
    rowcount = 1
    commamount = 0
    txtfile = open('ctl_details.txt', 'w')
    for row in worksheet.rows:
        rowstring = ''
        for cell in row:
            if cell.value == None:
                c = ''
            else:
                if cell.column == 'H' and worksheet['G'+str(rowcount)].value is not None and worksheet['F'+str(rowcount)].value is not None:
                    if type(worksheet['G'+str(rowcount)].value) is float or type(worksheet['G'+str(rowcount)].value) is int and (type(worksheet['F'+str(rowcount)].value) is float or type(worksheet['F'+str(rowcount)].value) is int):
                        c = worksheet['G'+str(rowcount)].value * worksheet['F'+str(rowcount)].value
                        commamount = commamount + c
                    else:
                        if 'COMM' != cell.value:
                            c = cell.value
                            commamount = commamount + c
                        else:
                            c = cell.value
                else:
                    c = cell.value
            if type(c) is float or type(c) is int and cell.column != 'A':
                c = '%.2f' % c
            elif type(c) is datetime.datetime:
                c = str(c)[:10]
            rowstring = rowstring + "\"" + str(c) + "\", "
        rowstring = rowstring[:len(rowstring)-2]
        if 'Total Commission' in rowstring:
            global totalcomm
            #print rowstring
        if 'NUMBER' in rowstring:
            printhis = True
        if printhis == True:
            #if '"", "", "",' not in rowstring:
            if worksheet['E'+str(rowcount)].value is not None and 'NUMBER' not in rowstring:
                txtfile.write(rowstring)
                txtfile.write('\n')
        rowcount = rowcount + 1
    totalc = '%.2f' % commamount
    return float(totalc)
    txtfile.close()

def insertheader(stmtdate, fileid, totalcomm):
    cur.execute("INSERT INTO M_HEADER (FILEID, STATEMENT_DATE, STATEMENT_AMOUNT) VALUES ('"+ str(fileid) +"', TO_DATE('"+ str(stmtdate) +"','YYYY-MM-DD'),'%.2f')" % totalcomm)
    conn.commit()
    return 'HEADER ADDED SUCCESSFULLY'

def loadsqlloader():
    subprocess.call(['sqlldr', 'dbtest/dbtest@pidev control=\'' + path + 'details.ctl\' log=\'' + path + today + '_log.log\'  bad=\'' + path + today + '_bad.bad\''])
    return 'details successfully added'

def rcamount():
    cur.execute("select count(1), sum(comm) from M_DETAILS where fileid is NULL ")
    rca = cur.fetchmany()
    return rca

def updateheader(id, rc, ta):
    cur.execute("update M_HEADER set recordcount = %s, comm_amount = %.2f where fileid = %s" % (rc, ta, id))
    conn.commit()
    return "Header successfully updated."

def updatedetails(id):
    cur.execute("update M_DETAILS set fileid = %i where fileid is NULL" % id)
    conn.commit()
    return "Details successfully updated."

def checkprocessstatus(id):
    cur.execute("select case when statement_amount <> comm_amount then 'Check' when statement_amount=comm_amount then 'Good' else 'Error' end from m_header where fileid =" + str(id))
    stat = cur.fetchone()
    cur.execute("update M_CARRIERFILES set processstatus = '" + str(stat[0]) + "' where fileid =" + str(id))
    conn.commit()
    return stat[0]

fname = str(getfilename(fileid))
fetchblob(fname, fileid)
totalcomm = getexcel('out_' + fname)
arr_filename = fname.split(".")
stmtdate = arr_filename[0][6:8] + '-' + arr_filename[0][8:10] + '-' + arr_filename[0][10:12]

ttime = time.strptime(stmtdate, "%m-%d-%y")
stmtdate = str(ttime.tm_year) + '-' + str(ttime.tm_mon) + '-' + str(ttime.tm_mday)
print insertheader(stmtdate, fileid, totalcomm)
print loadsqlloader()
print updateheader(fileid, rcamount()[0][0], rcamount()[0][1])
print updatedetails(fileid)
print checkprocessstatus(fileid)