import cx_Oracle

def connect():
    dsn = cx_Oracle.makedsn(host='59.2.0.80', sid='pidev', port=1522)
    conn = cx_Oracle.connect(user='dbtest', password='dbtest', dsn=dsn)
    cur = conn.cursor()
    return cur, conn

cur, conn = connect()

file_data = 'ACSIA_013016.xlsx'
def getnexfileid():
    cur.execute("select M_CARRIERFILES_SEQ.nextval from dual")
    id = cur.fetchone()
    return id[0]

def insertfile(carfid, fileid, file_data):
    outfile = file_data
    arr_file = file_data.split("\\")

    file_data = arr_file[(len(arr_file))-1]
    arr_filename = file_data.split(".")

    filename = arr_filename[0]

    cur.execute("INSERT INTO m_carrierfiles (feedid , fileid, filename, carfile) VALUES (" + str(carfid) + ", " + str(fileid) +  ", '" + filename + "', empty_blob())")
    cur.execute('commit')

    file = open(outfile, 'r  b')
    content = file.read()
    file.close()
    blobvar = cur.var(cx_Oracle.BLOB)
    blobvar.setvalue(0, content)
    print fileid
    sqlStr = "update m_carrierfiles set carfile=:blobData where fileid = " + str(fileid)
    cur.setinputsizes(blobData=cx_Oracle.BLOB)
    cur.execute(sqlStr, {'blobData': blobvar})
    cur.execute('commit')

feeid = 1
if feeid != None:
    fileid = getnexfileid()
    insertfile(feeid, fileid, file_data)
    print "File has been added"