import openpyxl
import datetime

def getexcel(xfile):
    workbook = openpyxl.load_workbook(filename=xfile, use_iterators=True, data_only=True)
    worksheet = workbook.get_sheet_by_name('ACSIA INC')
    printthis = False
    txtfile = open('ctl_details.txt', 'w')
    lastrow = 0
    for row in worksheet.iter_rows():
        rowstring = ''
        for cell in row:
            if 'Total Comm' in str(cell.internal_value):
                lastrow = cell.row
            if lastrow+1 > 2 and cell.column is 'H':
                if cell.internal_value is not None:
                    return cell.internal_value
                if cell.internal_value is not None:
                    if cell.column is 'A':
                        if type(cell.internal_value) is float:
                            c = "%.0f" % cell.internal_value
                        else:
                            c = cell.internal_value
                    elif cell.column is 'B' or cell.column is 'E':
                        c = str(cell.internal_value)[:10]
                    elif cell.column is 'F' or cell.column is 'H':
                        if type(cell.internal_value) is float:
                            c = "%.2f" % cell.internal_value
                        else:
                            c = str(cell.internal_value)
                    else:
                        c = str(cell.internal_value)
                else:
                    if cell.column is 'E' and cell.internal_value is None:
                        c = "SKIP"
                    elif cell.column is 'F' and cell.internal_value is None:
                        c = "SKIP"
                    elif cell.column is 'G' and cell.internal_value is None:
                        c = "SKIP"
                    else:
                        c = ''
            rowstring = rowstring + "\"" + str(c) + "\", "
    if 'NUMBER' in rowstring:
        printthis = True
    if printthis == True:
        if rowstring.count('SKIP') == 0 and 'NUMBER' not in rowstring:
            print rowstring
            txtfile.write(rowstring[:len(rowstring)-2])
            txtfile.write('\n')
    txtfile.close()

excelfile = raw_input('XCL : ')
print getexcel('ACSIA_013015.xlsx')
