Page 1
Statement Date: March 14, 2015

Carrier Name: HealthPlus

Policy Number                    Insured Name    Commission Amount
Included
123456                           John C. Smith   12.45
789456                           Juan D. Cruz    89.89
    						 ______
						 102.34

Page 2

Policy Number                    Insured Name    Commission Amount
Included
123333                           John C. Smith   12.45
789555                           Juan D. Cruz    89.89
    						 ______
						 102.34

Skip
123551                           John C. Smith   12.45
789000                           Juan D. Cruz    89.89
    						 ______
						 102.34

Included
123551                           John C. Smith   12.45
789000                           Juan D. Cruz    89.89
    						 ______
						 102.34
