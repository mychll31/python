import subprocess
import time
import cx_Oracle

dsn = cx_Oracle.makedsn(host='59.2.0.80', sid='pidev', port=1522)
conn = cx_Oracle.connect(user='dbtest', password='dbtest', dsn=dsn)
cur = conn.cursor()

path = 'C:\Users\malcorin\Documents\PY\pdftotext\carrier'
lastid = 0
def convertpdftotext():
    subprocess.Popen(["pdftotext", "-layout", pdfname, txtname]).communicate()
    return 'File successfully converted'

def sliceword(content):
   if ':' in content:
       contentcol = content.index(':')
       return content[contentcol+2:]

def createtxt(txtname):
    with open(txtname) as f:
        contents = f.readlines()
    txtfile = open('ctl_output.txt', 'w')
    ctr = 0
    for content in contents:
        ctr = ctr + 1
        content = content.replace("\n", "")
        if ctr > 4:
            content = content.replace("  ", "\",\"")
            content = content.replace("\"\",", "")
            content = content.replace("\",\" ", "\",\"")
        else:
            content = content.replace("  ", "")
        txtfilecontent = '"' + str(content) + '"'

        if '\"\"' in txtfilecontent or '\"\"' in txtfilecontent:
            None
        elif ctr == 1:
            cardate = sliceword(content)
            ttime = time.strptime(cardate, "%B %d, %Y")
            cardate = str(ttime.tm_year) + '-' + str(ttime.tm_mon) + '-' + str(ttime.tm_mday)
        elif ctr == 3:
            carname = sliceword(content)
        elif ctr == 5:
            None
        else:
            txtfile.write(txtfilecontent + '\n')
    txtfile.close()
    if carname != '' and cardate != '':
        print insertheader(carname, cardate)

def insertheader(carname,cardate):
    cur.execute("insert into PDF_HEADER (NAME,STMTDATE) values ('" + carname + "', TO_DATE('" + cardate + "','YYYY-MM-DD'))")
    conn.commit()
    return "Header successfully added"

def getlastid():
    cur.execute("select pdfheader_seq.currval from dual")
    lastid = cur.fetchone()
    return lastid[0]

def createctl():
    ctlfile = open("details.ctl", "w")
    ctlfile.write("load data\n")
    ctlfile.write("infile '" + path + "\ctl_output.txt'\n")
    ctlfile.write("badfile '" + path + "\\bad.bad'\n")
    ctlfile.write("into table PDF_DETAILS\n")
    ctlfile.write("APPEND\n")
    ctlfile.write("fields terminated by \",\" optionally enclosed by '\"'\n")
    ctlfile.write("(POLICY_NO, INSURED_NAME, COMM_AMOUNT)")
    ctlfile.close()
    return "Control File Successfully created"

def loadsqlloader():
    subprocess.call(['sqlldr', 'dbtest/dbtest@pidev control=\'' + path + '\details.ctl\' log=\'' + path + '\log.log\''])
    return 'details successfully added'

def rcamount():
    cur.execute("select count(1), sum(comm_amount) from PDF_DETAILS where id is NULL ")
    rca = cur.fetchmany()
    return rca

def updateheader(id, rc, ta):
    cur.execute("update PDF_HEADER set recordcnt = %d, totalamount = %s where id = %i" % (rc, ta, id))
    conn.commit()
    return "Header successfully updated."

def updatedetails():
    cur.execute("update PDF_DETAILS set id = %i where id is NULL" % lastid)
    conn.commit()
    return "Details successfully updated."

if __name__ == "__main__":
    print 'Convert PDF to TEXT'
    pdfname = 'carrier_new.pdf'
    txtname = 'carrierText.txt'

    # print convertpdftotext()
    createtxt(txtname)
    lastid = getlastid()
    print lastid
    print createctl()
    print loadsqlloader()

    print updateheader(lastid,rcamount()[0][0],rcamount()[0][1])
    print updatedetails()
